# Fetches e-mails from an IMAP mailbox and sends them to a stashcat channel

Based on [stashcat-api-client](https://gitlab.com/aeberhardt/stashcat-api-client) by Anselm Eberhardt.


**Warning:** This is still a work in progress, so far only very basic text e-mails have been tested.

## Requirements:
- An IMAP mailbox (new mails will be fetched, marked as read and sent to the given stashcat channel)
- A stashcat account with write access to the target channel

## Prepare the Environment

```
python3 -m venv .venv
source .venv/bin/activate
pip3 install -r requirements.txt
```

## Setup config file

- Create imap2stashcat.cfg file (cf imap2stashcat.cfg.example)
- Add IMAP details (username, password, e-mail-address, server, port)
- Add stashcat details (username, password, encryption-key, channel)

## Run the script (e.g. in a cronjob)
`python3 imap2stashcat.py`
